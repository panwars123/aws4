provider "aws" {
  region = "us-east-1" 
}


# aws_vpc.main:
resource "aws_vpc" "main" {
#    arn                              = "arn:aws:ec2:us-east-1:732026571143:vpc/vpc-0771178064db20bf9"
    assign_generated_ipv6_cidr_block = false
    cidr_block                       = "10.0.0.0/16"
#    default_network_acl_id           = "acl-0953cbfd12858d153"
#    default_route_table_id           = "rtb-01bc2e2842aefbf82"
#    default_security_group_id        = "sg-06f3d41df36651acd"
#    dhcp_options_id                  = "dopt-76f4b80c"
    enable_classiclink               = false
    enable_classiclink_dns_support   = false
    enable_dns_hostnames             = false
    enable_dns_support               = true
#    id                               = "vpc-0771178064db20bf9"
    instance_tenancy                 = "default"
#    main_route_table_id              = "rtb-01bc2e2842aefbf82"
#    owner_id                         = "732026571143"
    tags                             = {
        "Name" = "myvpc"
    }
}
