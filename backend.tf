terraform {
  backend "s3" {
    bucket = "test4597san"
    key    =  "terraform.tfstate"
    region = "us-east-1"
  }
}
